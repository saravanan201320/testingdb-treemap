
var app = angular.module('myApp',['zingchart-angularjs','angular.filter']);

app.controller('MainController', function($scope,$http,$filter){
  $scope.ASGNames =[];

  $scope.show = function(){
    console.log("hiiiii")
  }

  $scope.RXHDApplicationName = [];
  $scope.RXHDBussinessCapabilityData = [];

  $scope.RXCoreApplicationName = [];
  $scope.RXCoreBussinessCapability = [];

  $scope.GroupApplicationName = [];
  $scope.GroupBussinessCapability = [];

  $scope.DentalApplicationName = [];
  $scope.DentalBussinessCapability = [];

  var getJSONData = $http.get("SampleData.json");
  getJSONData.success(function (data) {
    $scope.groups = data;
    console.log($scope.groups);
    for (var i = 0; i < data.ASG.length; i++){
      $scope.ASGNames.push(data.ASG[i].Group.groupName);
      if (data.ASG[i].Group.groupName == "Rx-HD") {
        $scope.RXHDApplicationName.push(data.ASG[i].Group.Application);

      };
      if (data.ASG[i].Group.groupName == "Rx-Core") {
        $scope.RXCoreApplicationName.push(data.ASG[i].Group.Application);
      };
      if (data.ASG[i].Group.groupName == "Group") {
        $scope.GroupApplicationName.push(data.ASG[i].Group.Application);
      };
      if (data.ASG[i].Group.groupName == "Dental") {
        $scope.DentalApplicationName.push(data.ASG[i].Group.Application);
      };
    }
    $scope.ASGNames = $filter('unique')($scope.ASGNames);

    $scope.RXHDApplicationName = $filter('unique')($scope.RXHDApplicationName);
    $scope.RXCoreApplicationName = $filter('unique')($scope.RXCoreApplicationName);
    $scope.GroupApplicationName = $filter('unique')($scope.GroupApplicationName);
    $scope.DentalApplicationName = $filter('unique')($scope.DentalApplicationName);

    $scope.RXHDApplicationNameCount = $scope.RXHDApplicationName.length;
    $scope.RXCoreApplicationNameCount = $scope.RXCoreApplicationName.length;
    $scope.GroupApplicationNameCount = $scope.GroupApplicationName.length;
    $scope.DentalApplicationNameCount = $scope.DentalApplicationName.length;
    $scope.TotalApplicationCount = $scope.RXHDApplicationName.length+$scope.RXCoreApplicationName.length+$scope.GroupApplicationName.length+$scope.DentalApplicationName.length;


    $scope.ASGData = [{"Rx-HD" : $scope.RXHDApplicationName.length,
      "Rx-Core": $scope.RXCoreApplicationName.length,
      "Group":$scope.GroupApplicationName.length,
      "Dental":$scope.DentalApplicationName.length,
      "Total": $scope.RXHDApplicationName.length+$scope.RXCoreApplicationName.length+$scope.GroupApplicationName.length+$scope.DentalApplicationName.length}];


    $scope.RXHDWidth = ($scope.RXHDApplicationNameCount/$scope.TotalApplicationCount)*100+"%";
    $scope.RXCoreWidth = ($scope.RXCoreApplicationNameCount/$scope.TotalApplicationCount)*100+"%";
    $scope.GroupWidth = ($scope.GroupApplicationNameCount/$scope.TotalApplicationCount)*100+"%";
    $scope.DentalWidth= ($scope.DentalApplicationNameCount/$scope.TotalApplicationCount)*100+"%";

    $scope.RXHDHeight = ($scope.RXHDApplicationNameCount/$scope.TotalApplicationCount)*750+"px";
    $scope.RXCoreHeight = ($scope.RXCoreApplicationNameCount/$scope.TotalApplicationCount)*750+"px";
    $scope.GroupHeight = ($scope.GroupApplicationNameCount/$scope.TotalApplicationCount)*750+"px";
    $scope.DentalHeight= ($scope.DentalApplicationNameCount/$scope.TotalApplicationCount)*750+"px";




    $scope.RXHDBussinessCapability = [];
    $scope.RXHDFunctionality = [];
    $scope.RXHDFunctionalityData = [];
    $scope.RXCoreBussinessCapabilityData = [];
    $scope.GroupBussinessCapabilityData = [];
    $scope.DentalBussinessCapabilityData = [];



    for (var i = 0; i< $scope.RXHDApplicationName.length; i++) {
      var RXHDApplication = "";
      var RXHDApplication =  $scope.RXHDApplicationName[i];
      console.log("==========================="+[i]);
      for (var j = 0; j < data.ASG.length; j++){

        if(data.ASG[j].Group.Application == RXHDApplication && data.ASG[j].Group.groupName == "Rx-HD"){

          $scope.RXHDBussinessCapability.push(data.ASG[j].Group.businessCapability);


        }
      }


      //$scope.RXHDBussinessCapability = $filter('orderBy')($scope.RXHDBussinessCapability);

      //console.log("------------>"+$scope.RXHDBussinessCapability.length);

      for(var x=0; x< $scope.RXHDBussinessCapability.length; x++){
        for (var y = 0; y < data.ASG.length; y++){
          console.log(data.ASG[y].Group.Application);
          console.log(RXHDApplication);
        }
        //console.log("businessCapability :"+data.ASG[x].Group.businessCapability);
        //console.log("businessCapability1 :"+$scope.RXHDBussinessCapability[x]);


        if(data.ASG[x].Group.businessCapability == $scope.RXHDBussinessCapability[x]){

          $scope.RXHDFunctionality.push(data.ASG[x].Group.functionality);
          //console.log($scope.RXHDFunctionality);
        }
      }


      $scope.RXHDBussinessCapability = $filter('unique')($scope.RXHDBussinessCapability);
      $scope.RXHDFunctionality = $filter('unique')($scope.RXHDFunctionality);
      //console.log("RXHDBussinessCapability :"+ $scope.RXHDBussinessCapability+" length :"+ $scope.RXHDBussinessCapability.length);
      //console.log("RXHDFunctionality :"+ $scope.RXHDFunctionality+" length :"+ $scope.RXHDFunctionality.length);
      $scope.RXHDBussinessCapabilityData.push({"text": RXHDApplication, "value": $scope.RXHDBussinessCapability.length});
      $scope.RXHDFunctionalityData.push({"text": $scope.RXHDBussinessCapability, "value": $scope.RXHDBussinessCapability.length});
      $scope.RXHDBussinessCapability = [];
      $scope.RXHDFunctionality = [];

    };

    for (var i = 0; i< $scope.RXCoreApplicationName.length; i++) {
      var RXCoreApplication = "";
      var RXCoreApplication =  $scope.RXCoreApplicationName[i];

      for (var j = 0; j < data.ASG.length; j++){

        if(data.ASG[j].Group.Application == RXCoreApplication && data.ASG[j].Group.groupName == "Rx-Core"){

          $scope.RXCoreBussinessCapability.push(data.ASG[j].Group.businessCapability);
          // console.log($scope.RXHDBussinessCapability);
        }
      }
      $scope.RXCoreBussinessCapability = $filter('unique')($scope.RXCoreBussinessCapability);
      $scope.RXCoreBussinessCapabilityData.push({"text": RXCoreApplication, "value": $scope.RXCoreBussinessCapability.length});
      $scope.RXCoreBussinessCapability = [];
    };

    for (var i = 0; i< $scope.GroupApplicationName.length; i++) {
      var GroupApplication = "";
      var GroupApplication =  $scope.GroupApplicationName[i];

      for (var j = 0; j < data.ASG.length; j++){

        if(data.ASG[j].Group.Application == GroupApplication && data.ASG[j].Group.groupName == "Group"){

          $scope.GroupBussinessCapability.push(data.ASG[j].Group.businessCapability);
          // console.log($scope.RXHDBussinessCapability);
        }
      }
      $scope.GroupBussinessCapability = $filter('unique')($scope.GroupBussinessCapability);
      $scope.GroupBussinessCapabilityData.push({"text": GroupApplication, "value": $scope.GroupBussinessCapability.length});
      $scope.GroupBussinessCapability = [];
    };

    for (var i = 0; i< $scope.DentalApplicationName.length; i++) {
      var DentalApplication = "";
      var DentalApplication =  $scope.DentalApplicationName[i];

      for (var j = 0; j < data.ASG.length; j++){

        if(data.ASG[j].Group.Application == DentalApplication && data.ASG[j].Group.groupName == "Dental"){

          $scope.DentalBussinessCapability.push(data.ASG[j].Group.businessCapability);
          // console.log($scope.RXHDBussinessCapability);
        }
      }

      $scope.DentalBussinessCapability = $filter('unique')($scope.DentalBussinessCapability);
      $scope.DentalBussinessCapabilityData.push({"text": DentalApplication, "value": $scope.DentalBussinessCapability.length});
      $scope.DentalBussinessCapability = [];
    };
    // $scope.RXHDBussinessCapability.push([{"applicationName": $scope.RXHDApplicationName, "businessCount": $scope.}])



    $scope.myObj = {
      "graphset":[
        {
          "type":"treemap",
          "plotarea":{
            "margin":"0 0 30 0"
          },
          "tooltip":{

          },
          "options":{

          },
          "series":[
            {
              "text":"ASG",

              "children":[
                {
                  "text":"RX-HD "+ "Overall Test Cases: "+ $scope.RXHDBussinessCapability.length,
                  "children":$scope.RXHDBussinessCapabilityData
                },
                {
                  "text":"RX-Core",
                  "children":$scope.RXCoreBussinessCapabilityData
                },
                {
                  "text":"Group",
                  "children":$scope.GroupBussinessCapabilityData
                },
                {
                  "text":"Dental",
                  "children":$scope.DentalBussinessCapabilityData
                }
              ]
            }
          ]
        }
      ]
    };

  });
});

/**
 * Created by saravana.sivakumar on 11/23/2015.
 */
